module Api
  module V1
    class ApiController < ApplicationController
      doorkeeper_for :all, :unless => lambda { request.format.xhr? or current_user }
      respond_to :json
    end
  end
end