module Api
  module V1
    class ParadaTieneSegmentosController < ApiController
      before_action :set_parada_tiene_segmento, only: [:show, :edit, :update, :destroy]
      # GET /parada_tiene_segmentos
      # GET /parada_tiene_segmentos.json
      def index
        @parada_tiene_segmentos = ParadaTieneSegmento.all
        respond_with @parada_tiene_segmentos
      end

      # GET /parada_tiene_segmentos/1
      # GET /parada_tiene_segmentos/1.json
      def show
        respond_with ParadaTieneSegmento.find(params[:id])
      end

      # GET /parada_tiene_segmentos/new
      def new
        @parada_tiene_segmento = ParadaTieneSegmento.new
      end

      # GET /parada_tiene_segmentos/1/edit
      def edit
      end

      # POST /parada_tiene_segmentos
      # POST /parada_tiene_segmentos.json
      def create
        @parada_tiene_segmento = ParadaTieneSegmento.new(parada_tiene_segmento_params)

        respond_to do |format|
          if @parada_tiene_segmento.save
            format.html { redirect_to @parada_tiene_segmento, notice: 'Parada tiene segmento was successfully created.' }
            format.json { render :show, status: :created, location: @parada_tiene_segmento }
          else
            format.html { render :new }
            format.json { render json: @parada_tiene_segmento.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /parada_tiene_segmentos/1
      # PATCH/PUT /parada_tiene_segmentos/1.json
      def update
        respond_to do |format|
          if @parada_tiene_segmento.update(parada_tiene_segmento_params)
            format.html { redirect_to @parada_tiene_segmento, notice: 'Parada tiene segmento was successfully updated.' }
            format.json { render :show, status: :ok, location: @parada_tiene_segmento }
          else
            format.html { render :edit }
            format.json { render json: @parada_tiene_segmento.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /parada_tiene_segmentos/1
      # DELETE /parada_tiene_segmentos/1.json
      def destroy
        @parada_tiene_segmento.destroy
        respond_to do |format|
          format.html { redirect_to parada_tiene_segmentos_url, notice: 'Parada tiene segmento was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_parada_tiene_segmento
          @parada_tiene_segmento = ParadaTieneSegmento.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def parada_tiene_segmento_params
          params.require(:parada_tiene_segmento).permit(:parada_id, :segmento_id)
        end
    end

  end
end
