module Api
  module V1
    class ParadasController < ApiController
      before_action :set_parada, only: [:show, :edit, :update, :destroy]
      # GET /paradas
      # GET /paradas.json
      def index
        @paradas = Parada.all
        respond_with @paradas
      end

      # GET /paradas/1
      # GET /paradas/1.json
      def show
        respond_with Parada.find(params[:id])
      end

      # GET /paradas/new
      def new
        @parada = Parada.new
      end

      # GET /paradas/1/edit
      def edit
      end

      # POST /paradas
      # POST /paradas.json
      def create
        @parada = Parada.new(parada_params)

        respond_to do |format|
          if @parada.save
            format.json { render :show, status: :created, location: @parada }
          else
            format.json { render json: @parada.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /paradas/1
      # PATCH/PUT /paradas/1.json
      def update
        respond_to do |format|
          if @parada.update(parada_params)
            format.json { render :show, status: :ok, location: @parada }
          else
            format.json { render json: @parada.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /paradas/1
      # DELETE /paradas/1.json
      def destroy
        @parada.destroy
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      def calculate_closest_stop
         # wgs84_proj4 = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'
         # wgs84_factory = RGeo::Geographic.spherical_factory(:srid => 4326, :proj4 => wgs84_proj4)
         # p = wgs84_factory.point(-84.08884599999999,9.890134) 
         respond_with  Parada.find_by_sql("SELECT a.desc, ST_AsText(a.punto_parada) AS punto_parada
          FROM paradas a 
          WHERE ST_DWithin (ST_GeomFromText('POINT(#{params[:long]} #{params[:lat]})', 4326), a.punto_parada, 5000) 
          AND a.activa = true
          ORDER BY ST_Distance(ST_GeomFromText('POINT(#{params[:long]} #{params[:lat]})', 4326), a.punto_parada) LIMIT 1")

      end

      def share_stop
        parada = Parada.new
        parada.punto_parada = "POINT(#{params[:lat]} #{params[:long]})"
        parada.desc = params[:desc] if params[:desc].present?
        if parada.save
         render :json  => parada.to_json
        else
          render :json  => {:errors => parada.errors.full_messages }
        end
      end
      
      private
        # Use callbacks to share common setup or constraints between actions.
        def set_parada
          @parada = Parada.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def parada_params
          params.require(:parada).permit(:desc, :ruta_img, :punto_parada, :activa)
        end
    end
  end
end