module Api
  module V1
    class ProvinciaController < ApiController
      before_action :set_provincium, only: [:show, :edit, :update, :destroy]
      # GET /provincia
      # GET /provincia.json
      def index
        @provincium = Provincia.all
        respond_with @provincia
      end

      # GET /provincia/1
      # GET /provincia/1.json
      def show
        respond_with Provincia.find(params[:id])
      end

      # GET /provincia/new
      def new
        @provincium = Provincia.new
      end

      # GET /provincia/1/edit
      def edit
      end

      # POST /provincia
      # POST /provincia.json
      def create
        @provincium = Provincia.new(provincium_params)

        respond_to do |format|
          if @provincium.save
            format.json { render :show, status: :created, location: @provincium }
          else
            format.json { render json: @provincium.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /provincia/1
      # PATCH/PUT /provincia/1.json
      def update
        respond_to do |format|
          if @provincium.update(provincium_params)
            format.json { render :show, status: :ok, location: @provincium }
          else
            format.json { render json: @provincium.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /provincia/1
      # DELETE /provincia/1.json
      def destroy
        @provincium.destroy
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_provincium
          @provincium = Provincia.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def provincium_params
          params.require(:provincia).permit(:nombre)
        end
    end
  end
end

