module Api
  module V1
    class RutaController < ApiController
      before_action :set_ruta, only: [:show, :edit, :update, :destroy]
      # GET /ruta
      # GET /ruta.json
      def index
        @ruta = Ruta.all
        respond_with @ruta
      end

      # GET /ruta/1
      # GET /ruta/1.json
      def show
        respond_with Ruta.find(params[:id])
      end

      # GET /ruta/new
      def new
        @rutum = Ruta.new
      end

      # GET /ruta/1/edit
      def edit
      end

      # POST /ruta
      # POST /ruta.json
      def create
        @rutum = Ruta.new(ruta_params)

        respond_to do |format|
          if @rutum.save
            format.html { redirect_to @rutum, notice: 'Ruta was successfully created.' }
            format.json { render :show, status: :created, location: @rutum }
          else
            format.html { render :new }
            format.json { render json: @rutum.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /ruta/1
      # PATCH/PUT /ruta/1.json
      def update
        respond_to do |format|
          if @rutum.update(ruta_params)
            format.html { redirect_to @rutum, notice: 'Ruta was successfully updated.' }
            format.json { render :show, status: :ok, location: @rutum }
          else
            format.html { render :edit }
            format.json { render json: @rutum.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /ruta/1
      # DELETE /ruta/1.json
      def destroy
        @rutum.destroy
        respond_to do |format|
          format.json { head :no_content }
        end
      end

       # Method for calculating the closest route
      def calculate_closest_route
         respond_with Ruta.find_by_sql("SELECT r.nombre, e.nombre as nombre_empresa, s.nombre as nombre_segmento, r.id as ruta_id, r.tarifa 
          FROM segmentos s, ruta r, empresas e, ruta_contiene_segmentos rcs 
          WHERE ST_DWithin (ST_GeomFromText('POINT(#{params[:long]} #{params[:lat]})', 4326), s.segmento_linea, 5000) 
          AND  s.id = rcs.segmento_id 
          AND rcs.ruta_id = r.id 
          AND r.\"Empresa_id\" = e.id ORDER BY ST_Distance (ST_GeomFromText(\'POINT(#{params[:long]} #{params[:lat]})\', 4326), s.segmento_linea) LIMIT 1")
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_ruta
          @rutum = Ruta.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def ruta_params
          params.require(:ruta).permit(:id, :nombre, :codigo, :tarifa, :Empresa_id, :ruta_linea)
        end
    end
  end
end