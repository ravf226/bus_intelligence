module Api
  module V1
    class SegmentosController < ApiController
      before_action :set_segmento, only: [:show, :edit, :update, :destroy]

      # GET /segmentos
      # GET /segmentos.json
      def index
        @segmento = Segmento.all
        @segmento = Ruta.find(params[:rutum_id]).segmentos if params[:rutum_id].present?
        respond_with @segmento
      end

      # GET /segmentos/1
      # GET /segmentos/1.json
      def show
        respond_with Segmento.find(params[:id])
      end

      # GET /segmentos/new
      def new
        respond_with Segmento.new
      end

      # GET /segmentos/1/edit
      def edit
      end

      # POST /segmentos
      # POST /segmentos.json
      def create
        @segmento = Segmento.new(segmento_params)

        respond_to do |format|
          if @segmento.save
            format.json { render :show, status: :created, location: @segmento }
          else
            format.json { render json: @segmento.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /segmentos/1
      # PATCH/PUT /segmentos/1.json
      def update
        respond_to do |format|
          if @segmento.update(segmento_params)
            format.json { render :show, status: :ok, location: @segmento }
          else
            format.json { render json: @segmento.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /segmentos/1
      # DELETE /segmentos/1.json
      def destroy
        @segmento.destroy
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      # Method for calculating the closest route
      def calculate_closest_segment
        respond_with Segmento.find_by_sql("SELECT s.nombre, ST_AsText(s.segmento_linea) AS segmento
          FROM segmentos s 
          WHERE ST_DWithin (ST_GeomFromText('POINT(#{params[:long]} #{params[:lat]})', 4326), s.segmento_linea, 5000) 
          ORDER BY ST_Distance (ST_GeomFromText('POINT(#{params[:long]} #{params[:lat]})', 4326), s.segmento_linea)LIMIT 1")
      end

      def calculate_closest_segment_with(lat, long)
        
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_segmento
          @segmento = Segmento.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def segmento_params
          params.require(:segmento).permit(:nombre, :segmento_linea)
        end
    end
  end
end
