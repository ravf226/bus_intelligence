class RutaContieneSegmentosController < ApplicationController
  before_action :set_ruta_contiene_segmento, only: [:show, :edit, :update, :destroy]

  # GET /ruta_contiene_segmentos
  # GET /ruta_contiene_segmentos.json
  def index
    @ruta_contiene_segmentos = RutaContieneSegmento.all
  end

  # GET /ruta_contiene_segmentos/1
  # GET /ruta_contiene_segmentos/1.json
  def show
  end

  # GET /ruta_contiene_segmentos/new
  def new
    @ruta_contiene_segmento = RutaContieneSegmento.new
  end

  # GET /ruta_contiene_segmentos/1/edit
  def edit
  end

  # POST /ruta_contiene_segmentos
  # POST /ruta_contiene_segmentos.json
  def create
    @ruta_contiene_segmento = RutaContieneSegmento.new(ruta_contiene_segmento_params)

    respond_to do |format|
      if @ruta_contiene_segmento.save
        format.html { redirect_to @ruta_contiene_segmento, notice: 'Ruta contiene segmento was successfully created.' }
        format.json { render :show, status: :created, location: @ruta_contiene_segmento }
      else
        format.html { render :new }
        format.json { render json: @ruta_contiene_segmento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ruta_contiene_segmentos/1
  # PATCH/PUT /ruta_contiene_segmentos/1.json
  def update
    respond_to do |format|
      if @ruta_contiene_segmento.update(ruta_contiene_segmento_params)
        format.html { redirect_to @ruta_contiene_segmento, notice: 'Ruta contiene segmento was successfully updated.' }
        format.json { render :show, status: :ok, location: @ruta_contiene_segmento }
      else
        format.html { render :edit }
        format.json { render json: @ruta_contiene_segmento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ruta_contiene_segmentos/1
  # DELETE /ruta_contiene_segmentos/1.json
  def destroy
    @ruta_contiene_segmento.destroy
    respond_to do |format|
      format.html { redirect_to ruta_contiene_segmentos_url, notice: 'Ruta contiene segmento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ruta_contiene_segmento
      @ruta_contiene_segmento = RutaContieneSegmento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ruta_contiene_segmento_params
      params.require(:ruta_contiene_segmento).permit(:ruta_id, :ruta_Empresa_id, :segmento_id)
    end
end
