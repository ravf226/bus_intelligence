class Parada < ActiveRecord::Base
	before_create lambda{|parada| parada.normalize_decimals} 

	def normalize_decimals
	  if self.punto_parada.present?
		lat = self.punto_parada.x.round(6)
		lng = self.punto_parada.y.round(6)
		normalized_stop = "POINT(#{lat} #{lng})"
	    self.punto_parada = normalized_stop
	  end
	end
end
