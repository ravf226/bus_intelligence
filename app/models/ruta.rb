class Ruta < ActiveRecord::Base
	belongs_to :empresa
	has_many :ruta_contiene_segmentos
  	has_many :segmentos, through: :ruta_contiene_segmentos
end
