class Segmento < ActiveRecord::Base
	has_many :ruta_contiene_segmentos
  	has_many :rutas, through: :ruta_contiene_segmentos

  	before_create lambda{|segmento| segmento.normalize_decimals} 

  	def normalize_decimals
  	  if self.segmento_linea.present?
        s = "LINESTRING("
        self.segmento_linea.points.each do |p|
          lng = p.x.round(6)
	        lat = p.y.round(6)
	      s += "#{lng} #{lat},"
        end
        s = s.chomp(',')
        s += ")"
        self.segmento_linea = s
      end
	end
end
