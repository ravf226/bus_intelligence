json.array!(@cantons) do |canton|
  json.extract! canton, :id, :id, :nombre, :provincia_id
  json.url canton_url(canton, format: :json)
end
