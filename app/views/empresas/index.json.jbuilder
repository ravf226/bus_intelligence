json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nombre, :desc, :website, :tel1, :tel2, :email, :dir
  json.url empresa_url(empresa, format: :json)
end
