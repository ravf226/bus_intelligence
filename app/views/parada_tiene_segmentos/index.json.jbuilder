json.array!(@parada_tiene_segmentos) do |parada_tiene_segmento|
  json.extract! parada_tiene_segmento, :id, :parada_id, :segmento_id
  json.url parada_tiene_segmento_url(parada_tiene_segmento, format: :json)
end
