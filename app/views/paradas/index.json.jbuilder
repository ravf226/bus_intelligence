json.array!(@paradas) do |parada|
  json.extract! parada, :id, :desc, :ruta_img, :punto_parada
  json.url parada_url(parada, format: :json)
end
