json.array!(@ruta_contiene_segmentos) do |ruta_contiene_segmento|
  json.extract! ruta_contiene_segmento, :id, :ruta_id, :ruta_Empresa_id, :segmento_id
  json.url ruta_contiene_segmento_url(ruta_contiene_segmento, format: :json)
end
