json.array!(@segmentos) do |segmento|
  json.extract! segmento, :id, :nombre, :segmento_linea
  json.url segmento_url(segmento, format: :json)
end
