Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  use_doorkeeper
  
  # Uncomment if you want default controllers in a NON Api schema

  #resources :parada_tiene_segmentos, :only => [:index, :show]

  #resources :paradas, :only => [:index, :show]

  #resources :segmentos, :only => [:index, :show]

  #resources :ruta_contiene_segmentos, :only => [:index, :show]

  #resources :ruta, :only => [:index, :show]

  #resources :provincia, :only => [:index, :show]

  #resources :empresas, :only => [:index, :show]

  #resources :distritos, :only => [:index, :show]

  #resources :cantons, :only => [:index, :show], :defaults => {:format => "json"}


  namespace :api do
    namespace :v1 do
      resources :parada_tiene_segmentos, :only => [:index, :show], :defaults => {:format => "json"}
      resources :paradas, :only => [:index, :show], :defaults => {:format => "json"}
      resources :segmentos, :only => [:index, :show], :defaults => {:format => "json"}
      resources :ruta_contiene_segmentos, :only => [:index, :show], :defaults => {:format => "json"}
      resources :ruta, :only => [:index, :show], :defaults => {:format => "json"} do
        resources :segmentos, :only => [:index, :show], :defaults => {:format => "json"}
      end
      resources :provincia, :only => [:index, :show], :defaults => {:format => "json"}
      resources :empresas, :only => [:index, :show], :defaults => {:format => "json"} do
        resources :ruta, :only => [:index, :show], :defaults => {:format => "json"}
      end
      resources :distritos, :only => [:index, :show], :defaults => {:format => "json"}
      resources :cantons, :only => [:index, :show], :defaults => {:format => "json"}
      # special routes
      get 'ruta/calculate_closest_route/:lat/:long', to: 'ruta#calculate_closest_route', constraints: { lat: /\-*\d+.\d+/ , long: /\-*\d+.\d+/ }, :defaults => {:format => "json"}
      get 'segmentos/calculate_closest_segment/:lat/:long', to: 'segmentos#calculate_closest_segment', constraints: { lat: /\-*\d+.\d+/ , long: /\-*\d+.\d+/ }, :defaults => {:format => "json"}
      get 'paradas/calculate_closest_stop/:lat/:long', to: 'paradas#calculate_closest_stop', constraints: { lat: /\-*\d+.\d+/ , long: /\-*\d+.\d+/ }, :defaults => {:format => "json"}
      get 'paradas/share_stop/:lat/:long(/:desc)', to: 'paradas#share_stop', constraints: { lat: /\-*\d+.\d+/ , long: /\-*\d+.\d+/ }, :defaults => {:format => "json"}
    end
  end
    #  match "/:ruta/near/:lat/:lng(/:range)", :to => "restaurants#near", :as => "near", :constraints => {:lat => /\-*\d+.\d+/ , :lng => /\-*\d+.\d+/ }

  devise_scope :user do
    root to: "devise/sessions#new"
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
