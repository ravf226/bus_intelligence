class CreateDistritos < ActiveRecord::Migration
  def change
    create_table :distritos do |t|
      t.integer :canton_id
      t.integer :canton_provincia_id
      t.integer :ruta_id
      t.integer :ruta_Empresa_id
      t.timestamps
    end
  end
end
