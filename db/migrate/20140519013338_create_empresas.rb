class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nombre
      t.string :desc
      t.string :website
      t.string :tel1
      t.string :tel2
      t.string :email
      t.string :dir

      t.timestamps
    end
  end
end
