class CreateRuta < ActiveRecord::Migration
  def change
    create_table :ruta do |t|
      t.string :nombre
      t.string :codigo
      t.string :tarifa
      t.integer :Empresa_id
      t.timestamps
    end
  end
end
