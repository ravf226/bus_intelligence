class CreateRutaContieneSegmentos < ActiveRecord::Migration
  def change
    create_table :ruta_contiene_segmentos do |t|
      t.integer :ruta_id
      t.integer :ruta_Empresa_id
      t.integer :segmento_id

      t.timestamps
    end
  end
end
