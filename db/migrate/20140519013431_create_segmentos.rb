class CreateSegmentos < ActiveRecord::Migration
  def change
  	wgs84_proj4 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
    create_table :segmentos do |t|
      t.string :nombre
      t.line_string :segmento_linea, :srid => 4326, :proj4 => wgs84_proj4
      t.timestamps
    end
  end

  def self.down
    drop_table :segmentos
  end
  
end
