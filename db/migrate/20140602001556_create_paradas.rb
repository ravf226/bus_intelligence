class CreateParadas < ActiveRecord::Migration
  def change
    wgs84_proj4 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
    create_table :paradas do |t|
      t.string :desc
      t.string :ruta_img
      t.point :punto_parada, :srid => 4326, :proj4 => wgs84_proj4

      t.timestamps
    end
  end
end
