class CreateParadaTieneSegmentos < ActiveRecord::Migration
  def change
    create_table :parada_tiene_segmentos do |t|
      t.integer :parada_id
      t.integer :segmento_id

      t.timestamps
    end
  end
end
