class AddCodeAndAreaAndPopulationToCantons < ActiveRecord::Migration
  def change
    add_column :cantons, :code, :integer
    add_column :cantons, :area, :float
    add_column :cantons, :population, :integer
  end
end
