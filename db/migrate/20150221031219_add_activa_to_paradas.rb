class AddActivaToParadas < ActiveRecord::Migration
  def change
    add_column :paradas, :activa, :boolean, :default => false
  end
end
