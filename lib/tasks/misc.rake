namespace :misc do
  desc "Set all active stops in 1"
  task set_active_stops: :environment do
    s = ''
    Parada.all.each do |p|
    	p.activa = true;
    	p.save ? s = "#{p.desc} fue activada " : s = "#{p.desc} NO fue activada "
        puts s
    end
  end

  desc "Round to 6 decimals all coordinates of all tables"
  task round_geom_6_coordinates: :environment do
    s = ''
    Parada.all.each do |p|
    	text = p.punto_parada
    	p.save ? s = "#{p.desc} fue activada " : s = "#{p.desc} NO fue activada "
        puts s
    end
  end

end