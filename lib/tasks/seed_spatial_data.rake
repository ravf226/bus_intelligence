namespace :seed_spatial_data do
  desc "Seed spatial database"
  task seed_spatial_database: :environment do
  	  	read_csv_and_seed_model("Empresa", "empresas.csv")
		puts "Empresa seeded succesfully"
		read_csv_and_seed_model("Provincia", "provincias.csv")
		puts "Provincia seeded succesfully"
		read_csv_and_seed_model("Distrito", "distritos.csv")
		puts "Distrito seeded succesfully"
		read_csv_and_seed_model("Canton", "cantones.csv")
		puts "Canton seeded succesfully"
		read_csv_and_seed_model("Parada", "paradas.csv")
		puts "Parada seeded succesfully"
		read_csv_and_seed_model("Segmento", "segmentos.csv")
		puts "Segmento seeded succesfully"
		# Rutas
		e = Empresa.find_by_nombre('Coopana')
		ruta1 = Ruta.find_or_create_by(nombre: "Ruta San Jose - Tibás Florida por Tibás") do |r|
		  r.codigo = ''
		  r.tarifa = 230
		  r.Empresa_id = e.id
		end

		e = Empresa.find_by_nombre('RapidosHeredianos')
		ruta2 = Ruta.find_or_create_by(nombre: "Ruta San Jose - Heredia") do |r|
		  r.codigo = ''
		  r.tarifa = 575
		  r.Empresa_id = e.id
		end

		e = Empresa.find_by_nombre('Coesa')
		ruta3 = Ruta.find_or_create_by(nombre: "Ruta San Jose - San Pedro") do |r|
		  r.codigo = ''
		  r.tarifa = 255
		  r.Empresa_id = e.id
		end

		# Now we relate routes with segments
		# San Jose Tibas
		ruta1.segmentos << Segmento.find_by(nombre: "San Jose-Radisson")
		ruta1.segmentos << Segmento.find_by(nombre: "Radisson-Cinco Esquinas")
		ruta1.segmentos << Segmento.find_by(nombre: "Cinco-Esquinas-POPS Tibas")
		ruta1.segmentos << Segmento.find_by(nombre: "POPS-Tibas-Antojitos-Tibas")
		ruta1.segmentos << Segmento.find_by(nombre: "Antojitos-MasxMenosTibas")
		ruta1.segmentos << Segmento.find_by(nombre: "Calle Central de San Jose-Calle 1 San Juan Tibas")
		ruta1.segmentos << Segmento.find_by(nombre: "Calle 1 San Juan-MacDonalds Tibas")
		ruta1.segmentos << Segmento.find_by(nombre: "Avenida 63, Pali-Avenida 63 Rostipollos")
		ruta1.segmentos << Segmento.find_by(nombre: "Avenida 63,Rostipollos-Terminal de Autobuses Coopana R.L Florida")
		# San Jose Heredia
		ruta2.segmentos << Segmento.find_by(nombre: "San Jose-Radisson")
		ruta2.segmentos << Segmento.find_by(nombre: "Radisson-Cinco Esquinas")
		ruta2.segmentos << Segmento.find_by(nombre: "Cinco-Esquinas-POPS Tibas")
		ruta2.segmentos << Segmento.find_by(nombre: "POPS-Tibas-Antojitos-Tibas")
		ruta2.segmentos << Segmento.find_by(nombre: "Antojitos-MasxMenosTibas")
		ruta2.segmentos << Segmento.find_by(nombre: "Calle Central de San Jose-Calle 1 San Juan Tibas")
		ruta2.segmentos << Segmento.find_by(nombre: "Calle 1 San Juan-Avenida 65 San Juan")
		ruta2.segmentos << Segmento.find_by(nombre: "Avenida 65 Pinturas Sur, San Juan-Avenida 65 Estadio Municipal San Juan")
		ruta2.segmentos << Segmento.find_by(nombre: "Avenida 65 Estadio Municipal-Plaza Santo Domingo")
		ruta2.segmentos << Segmento.find_by(nombre: "Plaza Santo Domingo-Parque Santo Domingo")
		ruta2.segmentos << Segmento.find_by(nombre: "Parque Santo Domingo-Mas x Menos San Pablo")
		ruta2.segmentos << Segmento.find_by(nombre: "Mas x Menos San Pablo-Universidad Nacional Heredia")
		# San Jose San pedro
		ruta3.segmentos << Segmento.find_by(nombre: "Terminal San Pedro-Parada Museo Nacional")
		ruta3.segmentos << Segmento.find_by(nombre: "Parada Museo Nacional-Parada La California")
		ruta3.segmentos << Segmento.find_by(nombre: "Parada La California-Parada Antigua Subaru")
		ruta3.segmentos << Segmento.find_by(nombre: "Antigua Subaru-Mall San Pedro ida")
		ruta3.segmentos << Segmento.find_by(nombre: "Antigua Subaru-Mall San Pedro ida")
		ruta3.segmentos << Segmento.find_by(nombre: "Parada Outlet-Parada Calle Real")



  end

  desc "Delete data from spatial database"
  task delete_database_data: :environment do
  	Empresa.delete_all
  	Provincia.delete_all
  	Canton.delete_all
  	Distrito.delete_all
  	Parada.delete_all
  	Segmento.delete_all
  	RutaContieneSegmento.delete_all
  end

  desc "process data from spatial database"
  task process_database_data: :environment do
  	process_segmento_csv("segmentos.csv", "result.csv")
  end

  	def read_csv_and_seed_model(model = "", file_name = "")
 		raise "The file_name argument is required" if file_name.blank?
	    raise "The model name must be supplied" if model.blank?
	    parsed_model = model.constantize
	    model_params_hash = {}
	    file_path = Rails.root.join('db/seeds', file_name)
	    raise "File not found: #{file}" unless File.exists?(file_path)
	    criteria_file = File.new(file_path, "r")
   		criteria_data = CSV.parse(criteria_file, {:headers => true, :col_sep => ',', :header_converters => :symbol})
   		criteria_data.each do |d|
   			# we create the hash
   			model_params_hash = d.to_hash
   			parsed_model.inspect
   			puts "#{model_params_hash.inspect} saved succesfully" if parsed_model.create!(model_params_hash)
   		end
	end

	def process_segmento_csv(file_name, result)
		file_path = Rails.root.join('db/seeds', file_name)
	    raise "File not found: #{file}" unless File.exists?(file_path)
		raise "The file_name argument is required" if file_name.blank?
		criteria_file = File.new(file_path, "r")
		file_path = Rails.root.join('db/seeds', result)
   		criteria_file2 = File.new(file_path, "r")
   		criteria_data = CSV.parse(criteria_file, {:headers => true, :col_sep => ',', :header_converters => :symbol})
   		CSV.open( criteria_file2, 'w' ) do |writer|
	   		writer << ["nombre", "segmento_linea"]
	   		criteria_data.each do |d|
	   			line_string = d[:segmento_linea]
	   			s = line_string.split("(")
	   			puts "S ************** "
	   			puts s.inspect
	   			s1 = s[1].split(")")
	   			puts "S1 ************** "
	   			puts s1.inspect
	   			s2 = s1[0].split(",")
	   			puts "S2 ************** "
	   			puts s2.inspect
	   			s3 = s2.map{|a| a.split(" ")}
	   			puts "S3 ************** "
	   			puts s3.inspect
	   			s4 = s3.map{|a| a.reverse }
	   			puts "S4 ************** "
	   			puts s4.inspect
	   			s5 = s4.map{|a| a.join(" ")}
	   			puts "S5 ************** "
	   			puts s5.inspect
	   			s6 = s5.join(",")
	   			puts "S6 ************** "
	   			puts s6.inspect
	   			writer << [d[:nombre], s6] 
	   		end
	   	end
	end

end
