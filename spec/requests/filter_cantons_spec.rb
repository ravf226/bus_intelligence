require "json"
require "selenium-webdriver"

include RSpec::Expectations

describe "FilterCantonsSpec" do

  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "http://ec2-54-85-101-32.compute-1.amazonaws.com/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 30
    @verification_errors = []
  end
  
  after(:each) do
    @driver.quit
    @verification_errors.should == []
  end
  
  it "test_filter_cantons_spec" do
    @driver.get(@base_url + "/")
    @driver.find_element(:id, "user_email").clear
    @driver.find_element(:id, "user_email").send_keys "ravf.226@gmail.com"
    @driver.find_element(:id, "user_password").clear
    @driver.find_element(:id, "user_password").send_keys "administrador12345"
    @driver.find_element(:name, "commit").click
    @driver.find_element(:css, "span.show > a.pjax").click
    @driver.find_element(:name, "query").clear
    @driver.find_element(:name, "query").send_keys "san"
    @driver.find_element(:css, "button.btn.btn-primary").click
    sleep(5)
    verify { (@driver.find_element(:css, "td.nombre_field.string_type").text).should == "Santa Cruz" }
    verify { (@driver.find_element(:xpath, "//form[@id='bulk_form']/table/tbody/tr[2]/td[3]").text).should == "San Pablo" }
    verify { (@driver.find_element(:xpath, "//form[@id='bulk_form']/table/tbody/tr[3]/td[3]").text).should == "San Isidro" }
    verify { (@driver.find_element(:xpath, "//form[@id='bulk_form']/table/tbody/tr[4]/td[3]").text).should == "San Rafael" }
    verify { (@driver.find_element(:css, "div.clearfix.total-count").text).should == "11 cantons" }
    @driver.find_element(:css, "span.label.label-important").click
  end
  
  def element_present?(how, what)
    @driver.find_element(how, what)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    @driver.switch_to.alert
    true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
  rescue ExpectationNotMetError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
    if (@accept_next_alert) then
      alert.accept()
    else
      alert.dismiss()
    end
    alert_text
  ensure
    @accept_next_alert = true
  end
end
