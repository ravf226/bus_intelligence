require 'test_helper'

class ParadaTieneSegmentosControllerTest < ActionController::TestCase
  setup do
    @parada_tiene_segmento = parada_tiene_segmentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:parada_tiene_segmentos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create parada_tiene_segmento" do
    assert_difference('ParadaTieneSegmento.count') do
      post :create, parada_tiene_segmento: { parada_id: @parada_tiene_segmento.parada_id, segmento_id: @parada_tiene_segmento.segmento_id }
    end

    assert_redirected_to parada_tiene_segmento_path(assigns(:parada_tiene_segmento))
  end

  test "should show parada_tiene_segmento" do
    get :show, id: @parada_tiene_segmento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @parada_tiene_segmento
    assert_response :success
  end

  test "should update parada_tiene_segmento" do
    patch :update, id: @parada_tiene_segmento, parada_tiene_segmento: { parada_id: @parada_tiene_segmento.parada_id, segmento_id: @parada_tiene_segmento.segmento_id }
    assert_redirected_to parada_tiene_segmento_path(assigns(:parada_tiene_segmento))
  end

  test "should destroy parada_tiene_segmento" do
    assert_difference('ParadaTieneSegmento.count', -1) do
      delete :destroy, id: @parada_tiene_segmento
    end

    assert_redirected_to parada_tiene_segmentos_path
  end
end
