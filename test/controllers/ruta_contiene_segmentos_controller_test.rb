require 'test_helper'

class RutaContieneSegmentosControllerTest < ActionController::TestCase
  setup do
    @ruta_contiene_segmento = ruta_contiene_segmentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ruta_contiene_segmentos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ruta_contiene_segmento" do
    assert_difference('RutaContieneSegmento.count') do
      post :create, ruta_contiene_segmento: { ruta_Empresa_id: @ruta_contiene_segmento.ruta_Empresa_id, ruta_id: @ruta_contiene_segmento.ruta_id, segmento_id: @ruta_contiene_segmento.segmento_id }
    end

    assert_redirected_to ruta_contiene_segmento_path(assigns(:ruta_contiene_segmento))
  end

  test "should show ruta_contiene_segmento" do
    get :show, id: @ruta_contiene_segmento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ruta_contiene_segmento
    assert_response :success
  end

  test "should update ruta_contiene_segmento" do
    patch :update, id: @ruta_contiene_segmento, ruta_contiene_segmento: { ruta_Empresa_id: @ruta_contiene_segmento.ruta_Empresa_id, ruta_id: @ruta_contiene_segmento.ruta_id, segmento_id: @ruta_contiene_segmento.segmento_id }
    assert_redirected_to ruta_contiene_segmento_path(assigns(:ruta_contiene_segmento))
  end

  test "should destroy ruta_contiene_segmento" do
    assert_difference('RutaContieneSegmento.count', -1) do
      delete :destroy, id: @ruta_contiene_segmento
    end

    assert_redirected_to ruta_contiene_segmentos_path
  end
end
